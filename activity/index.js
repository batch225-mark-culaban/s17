/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

alert("Hello, User!");




function printWelcomeMessages() {
	let firstName = prompt("Enter Your First Name");
	let lastName = prompt("Enter Your Last Name");
	let age = prompt("Enter Your Age");
	let place = prompt("Enter Your Location")
	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("You are " + age + "years old.");
	console.log("You are from" + place );
}

printWelcomeMessages();
//alert("Thank you for the input");

	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.


*/
alert("Your Top 5 Favorite Bands")

function topBands() {
	let first = prompt("Enter Your First Choice");
	let second = prompt("Enter Your Second Choice");
	let third = prompt("Enter Your Third Choice");
	let fourth = prompt("Enter Your Fourth Choice");
	let last = prompt("Enter Your Last Choice");
	console.log("Your Top 5 Bands are");
	console.log("1. " + first );
	console.log("2. " + second );
	console.log("3. " + third );
	console.log("4. " + fourth );
	console.log("5. " + last );
}

topBands();





/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
alert("Your Top 5 Movies");
	function topMovies() {
	let first = prompt("Enter Your Top 1");
	let firstrating = prompt("enter rating");
	let second = prompt("Enter Your Top 2");
	let secondrating = prompt("enter rating");
	let third = prompt("Enter Your Top 3");
	let thirdrating = prompt("enter rating");
	let fourth = prompt("Enter Your Top 4");
	let fourthrating = prompt("enter rating");
	let last = prompt("Enter Your Top 5");
	let lastrating = prompt("enter rating");
	console.log("Your Top 5 Movies are");
	console.log("1. " + first );
	console.log("ratings: " + firstrating);
	console.log("2. " + second );
	console.log("ratings: " + secondrating);
	console.log("3. " + third );
	console.log("ratings: " + thirdrating);
	console.log("4. " + fourth );
	console.log("ratings: " + fourthrating);
	console.log("5. " + last );
	console.log("ratings: " + lastrating);
}

topMovies();
	




/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printUsers();

