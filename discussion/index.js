console.log("hello world")

//  Basic Functions

// [SECTION] Function Declaration

/*
	The statements and instructions inside a function is not immediately executed when the function is defined.
	They are run/executed when a function is invoked.
	To invoke a declared function, add the name of the function and a parenthesis.

*/
/*
 Hoisting - javascript behavior for certain variables and functions to run to use them before their declaration
*/

printName()

function printName() {
	console.log("My name is John")
}



printName();

//Function Expression
let variableFunction = function myGreetings() {
console.log("Hello")}


//Reassigning functions

variableFunction = function myGreetings() {
	console.log("Updated Hello");
}


variableFunction();

//Function Scoping
//Scope is the accessibility(visibility) of variables
/* 
  a. local or block scope
  b. global scope
  c. function scope
 */



//Local Variable
{
	let localVar = "Armando Perez";
 	console.log(localVar);
}

let globalVar = "Mr. Worldwide";

console.log(globalVar);


//Function Scope

function showNames() {

		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionConst);
		console.log(functionLet);

}

showNames();
//console.log(functionConst); //Result Error
//console.log(functionLet);//errror

//alert() and prompt()

//alert() - allows us to show a small  window at the top of our browser page to show information to our users

//It, much like alert(), will have the page wait until the user completes or enters their input.



alert("Hello, User!"); // same sa showSampleAlert()

function showSampleAlert() {
	alert("Hello User!");
}


showSampleAlert(); //Alternative sa babaw

// Prompt() - allows us to show a small window at the top of the browser to gather user input.


let samplePrompt = prompt("Enter you Name: ");
console.log("Hello, " + samplePrompt);


function printWelcomeMessages() {
	let firstName = prompt("Enter Your First Name");
	let lastName = prompt("Enter Your Last Name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to Mobile Legends");
}

printWelcomeMessages();







printUsers();
let printFriends() = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = alert("Enter your first friend's name:"); 
	let friend2 = prom("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friends); 
};


console.log(friend1);
console.log(friend2);





